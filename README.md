Accompanying Unity project to be used with [3D Dense Captioning](https://gitlab.ub.uni-bielefeld.de/markus.rothgaenger/3-dance)

Clone this repository and open it using the Unity Hub. It should automatically detect and install Unity version 2022.2.7f1.

Open the project, load the SampleScene and follow the instructions in the [3D Dense Captioning](https://gitlab.ub.uni-bielefeld.de/markus.rothgaenger/3-dance) README.