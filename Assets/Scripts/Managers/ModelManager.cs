using System;
using System.Collections.Generic;
using System.IO;
using Agent;
using Dummiesman;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Windows;

namespace Managers
{
    public class ModelManager : MonoBehaviour
    {
        public string BasePath;
        public GameObject ModelContainer;
        public RandomCameraAgent CameraAgent;
        public int ModelCount = 1000;
        public int ImageCount = 10;
        public bool ModelClassSort;
        public int ModelStartIndex;

        private GameObject currentModelInstance;
        private DirectoryInfo baseDirectoryInfo;
        private List<string> modelIds = new List<string>();
        private int currentModelIdx;
        private OBJLoader modelLoader = new OBJLoader();
        private bool takePhoto;
        private int currentModelCount;
        private int currentImageCount;
        
        private void Awake()
        {
            currentModelIdx = ModelStartIndex;
            currentModelCount = ModelStartIndex;
            // scan basepath, save all subdir ids in modelIds
            baseDirectoryInfo = new DirectoryInfo(BasePath);
            var subDirs = baseDirectoryInfo.GetDirectories();

            foreach (var dir in subDirs)
            {
                modelIds.Add(dir.Name);                
            }
        }

        private void Start()
        {
            LoadNewModel();
        }

        private void Update()
        {
            if (takePhoto || currentModelCount >= ModelCount)
            {
                return;
            }

            if (currentModelCount < ModelCount)
            {
                if (currentImageCount < ImageCount)
                {
                    CameraAgent.SetRandomCameraPosition();
                    takePhoto = true;
                    return;
                }
                
                LoadNewModel();
                currentImageCount = 0;
                currentModelCount += 1;
            }
        }

        private void LateUpdate()
        {
            if (takePhoto)
            {
                if (ModelClassSort)
                {
                    CameraAgent.SaveRGBModelSort(currentModelCount, currentImageCount);
                }
                else
                {
                    CameraAgent.SaveRGB(currentModelCount, currentImageCount);
                }
                currentImageCount += 1;
                takePhoto = false;
            }
        }

        private void LoadNewModel()
        {
            Resources.UnloadUnusedAssets();
            
            if (currentModelInstance is not null)
            {
                Destroy(currentModelInstance);
            }
            if (currentModelIdx >= modelIds.Count)
            {
                throw new Exception("reached end of model list");
            }
            var modelPath = $"{Path.Join(BasePath, modelIds[currentModelIdx])}/models/model_normalized.obj";

            try
            {
                currentModelInstance = new OBJLoader().Load(modelPath);
            }
            catch (FileNotFoundException e)
            {
                Debug.LogWarning($"file not found, skipping {modelPath}");
                currentModelIdx += 1;
                LoadNewModel();
                return;
            }
            currentModelInstance.transform.parent = ModelContainer.transform;
            CameraAgent.subject = currentModelInstance;
            
            // modelLoader.Materials.Clear();
            // modelLoader.Normals.Clear();
            // modelLoader.UVs.Clear();
            // modelLoader.Vertices.Clear();

            currentModelIdx++;
        }
    }
}