using System;
using UnityEngine;

namespace Utils
{
    [ExecuteInEditMode]
    public class FpsLimit : MonoBehaviour
    {
        public int FPS = 60;
        private void Awake()
        {
            Application.targetFrameRate = FPS;
        }
    }
}