using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using UnityEngine;

namespace Socket
{
    [Serializable]
    public enum MessageType
    {
        POS = 0,
        NEXT_MODEL = 1,
        UNKNOWN = 2,
    }

    [Serializable]
    public abstract class MessageData
    {
    }

    [Serializable]
    public class CamPos : MessageData
    {
        public float x;
        public float y;
        public float z;
    }

    [Serializable]
    public class InMessage
    {
        public MessageType type;
        public string data;
    }

    [Serializable]
    public class Hit
    {
        public int i;
        public int j;
        public List<float> worldPos;
    }

    [Serializable]
    public class PointCloudData
    {
        public bool finished;
        public string rgbPath;
        public int width;
        public int height;
        public List<Hit> points;
    }

    public class Client : MonoBehaviour
    {
        public string Host;
        public int Port;

        private TcpClient client;
        private readonly ConcurrentQueue<string> ingoing = new();
        private readonly ConcurrentQueue<string> outgoing = new();
        private Thread receiveThread;
        private NetworkStream stream;

        // Start is called before the first frame update
        private async void Start()
        {
            client = new TcpClient();
            try
            {
                await client.ConnectAsync(Host, Port);
                stream = client.GetStream();
                Debug.Log("client connected!");
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            receiveThread = new Thread(ReceiveThread)
            {
                IsBackground = true
            };
            receiveThread.Start();
        }

        private async void Update()
        {
            if (!outgoing.TryDequeue(out var outData)) return;

            outData = $"<START>{outData}<END>";
            var byteData = Encoding.UTF8.GetBytes(outData);
            // TODO: restrict write buffer size
            await stream.WriteAsync(byteData, 0, byteData.Length);
            Debug.Log($"sent {byteData.Length} bytes of data");
        }

        private void OnDisable()
        {
            receiveThread.Abort();
            stream.Close();
            client.Close();
        }

        public void EnqueueOutgoing(PointCloudData data)
        {
            Debug.Log("enqueuing");
            outgoing.Enqueue(
                item: JsonUtility.ToJson(data)
            );
        }

        public bool TryGetIngoingMessage(out MessageData messageData, out MessageType type)
        {
            messageData = null;
            type = MessageType.UNKNOWN;

            if (!ingoing.TryDequeue(out var data))
                return false;

            var message = JsonUtility.FromJson<InMessage>(data);
            type = message.type;

            switch (message.type)
            {
                case MessageType.POS:
                    messageData = JsonUtility.FromJson<CamPos>(message.data);
                    break;
                case MessageType.NEXT_MODEL:
                    break;
            }
            
            return true;
        }

        private async void ReceiveThread()
        {
            var readBuffSize = client.ReceiveBufferSize;
            Debug.Log($"receive thread started! {readBuffSize}");

            while (true)
            {
                if (!stream.DataAvailable)
                {
                    Thread.Sleep(2000);
                    continue;
                }

                var buff = new byte[readBuffSize];
                var jsonDataBuilder = new StringBuilder();

                int readBytes;
                do
                {
                    readBytes = await stream.ReadAsync(buff, 0, readBuffSize);
                    jsonDataBuilder.Append(Encoding.UTF8.GetString(buff, 0, readBytes));
                } while (readBytes > 0 && stream.DataAvailable);

                Debug.Log(jsonDataBuilder.ToString());
                var jsonObjects = jsonDataBuilder.ToString().Split("<END>");
                foreach (var json in jsonObjects)
                {
                    if (string.IsNullOrEmpty(json))
                        continue;

                    ingoing.Enqueue(json);
                    Debug.Log($"received json data {json}");
                }
            }
        }
    }
}