using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using Socket;
using UnityEngine;
using UnityEngine.Events;
using Random = System.Random;


namespace Agent
{
    public enum CameraAction
    {
        None,
        SaveRGBImage,
        SaveDepthImage,
        GetPointCloudData,
        NextPose,
        End
    }

    public class CameraAgent : MonoBehaviour
    {
        public Camera cam;
        public GameObject subject;
        public Client client;
        public string filePath;
        public UnityEvent<PointCloudData> pointCloudDataReady = new();

        private int step;
        private bool needsDepth;
        private Random rng = new Random();

        private string SaveRGB()
        {
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            var path =
                $"{Path.Combine(filePath, ((DateTimeOffset)DateTime.Now).ToUnixTimeMilliseconds().ToString())}.png";
            ScreenCapture.CaptureScreenshot(path);
            return path;
        }

        private PointCloudData CollectPointCloudData()
        {
            var data = new PointCloudData
            {
                height = cam.pixelHeight,
                width = cam.pixelWidth,
                points = new List<Hit>()
            };

            for (var i = 0; i < cam.pixelHeight; i++)
            {
                for (var j = 0; j < cam.pixelWidth; j++)
                {
                    var ray = cam.ScreenPointToRay(new Vector3(j, i));
                    // this assumes all relevant objects have mesh colliders attached..
                    var hasHit = Physics.Raycast(ray, out var hit);
                    if (!hasHit)
                    {
                        continue;
                    }


                    data.points.Add(new Hit()
                    {
                        i = i,
                        j = j,
                        worldPos = new List<float> { hit.point.x, hit.point.y, hit.point.z }
                    });
                }
            }

            data.finished = true;
            return data;
        }

        private void UpdateCameraPosition(CamPos positionData)
        {
            transform.position = new Vector3(positionData.x, positionData.y, positionData.z);
            transform.LookAt(subject.transform.position);
            cam.backgroundColor = new Color(
                (float)rng.NextDouble(), 
                (float)rng.NextDouble(), 
                (float)rng.NextDouble()
            ) * 0.5f;

            var rgbPath = SaveRGB();
            var pointCloudData = CollectPointCloudData();
            pointCloudData.rgbPath = rgbPath;
            client.EnqueueOutgoing(pointCloudData);
        }

        public void Update()
        {
            if (!client.TryGetIngoingMessage(out var messageData, out var type)) return;

            switch (type)
            {
                case MessageType.POS:
                    UpdateCameraPosition((CamPos)messageData);
                    break;
                case MessageType.NEXT_MODEL:
                    Debug.Log("LOAD NEXT MODEL");
                    throw new NotImplementedException();
                    break;
            }            
        }
    }
}