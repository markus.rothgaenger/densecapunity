using System;
using System.IO;
using UnityEngine;
using Random = System.Random;

namespace Agent
{
    public enum CamPosClass
    {
        FRONT_RIGHT = 0,
        FRONT_LEFT = 1,
        BACK_LEFT = 2,
        BACK_RIGHT = 3
    }
    public class RandomCameraAgent : MonoBehaviour
    {
        public Camera cam;
        public GameObject subject;
        public string filePath;
        
        private Random rng = new Random();

        public void SetRandomCameraPosition()
        {
            var height = (float)rng.NextDouble() * 0.7f;
            var distance = 0.6f + (float)rng.NextDouble() * 1.0f;
            var rot = (float)rng.NextDouble() * 360.0f;

            var rotQuaternion = Quaternion.Euler(0, rot, 0);

            transform.position = Vector3.zero;
            transform.rotation = rotQuaternion;
            transform.Translate(new Vector3(0, height, -distance), Space.Self);
            transform.LookAt(subject.transform.position);
            
            cam.backgroundColor = new Color(
                (float)rng.NextDouble(), 
                (float)rng.NextDouble(), 
                (float)rng.NextDouble()
            ) * 0.5f;
        }

        public string SaveRGB(int modelIndex, int pictureIndex)
        {
            var camPos = cam.transform.position;
            var right = camPos.x > 0;
            var front = camPos.z < 0;

            var camPosClass = right && front ? CamPosClass.FRONT_RIGHT :
                !right && front ? CamPosClass.FRONT_LEFT :
                !right && !front ? CamPosClass.BACK_LEFT : CamPosClass.BACK_RIGHT;

            var classPath = Path.Combine(filePath, camPosClass.ToString());
            if (!Directory.Exists(classPath))
                Directory.CreateDirectory(classPath);

            var path =
                $"{Path.Combine(classPath , $"{modelIndex}_{pictureIndex}_{camPos.x:F3}_{camPos.y:F3}_{camPos.z:F3}_{((DateTimeOffset)DateTime.Now).ToUnixTimeMilliseconds().ToString()}")}.png";
            ScreenCapture.CaptureScreenshot(path);
            return path;
        }
        
        public string SaveRGBModelSort(int modelIndex, int pictureIndex)
        {
            var camPos = cam.transform.position;
            var right = camPos.x > 0;
            var front = camPos.z < 0;

            var camPosClass = right && front ? CamPosClass.FRONT_RIGHT :
                !right && front ? CamPosClass.FRONT_LEFT :
                !right && !front ? CamPosClass.BACK_LEFT : CamPosClass.BACK_RIGHT;

            var modelClassPath = Path.Combine(filePath, modelIndex.ToString(), camPosClass.ToString());
            if (!Directory.Exists(modelClassPath))
                Directory.CreateDirectory(modelClassPath);

            var path =
                $"{Path.Combine(modelClassPath , $"{pictureIndex}_{camPos.x:F3}_{camPos.y:F3}_{camPos.z:F3}_{((DateTimeOffset)DateTime.Now).ToUnixTimeMilliseconds().ToString()}")}.png";
            ScreenCapture.CaptureScreenshot(path);
            return path;
        }
    }
}